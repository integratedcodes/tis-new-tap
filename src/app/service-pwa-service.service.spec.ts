import { TestBed } from '@angular/core/testing';

import { ServicePwaServiceService } from './service-pwa-service.service';

describe('ServicePwaServiceService', () => {
  let service: ServicePwaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicePwaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
