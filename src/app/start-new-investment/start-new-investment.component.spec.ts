import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartNewInvestmentComponent } from './start-new-investment.component';

describe('StartNewInvestmentComponent', () => {
  let component: StartNewInvestmentComponent;
  let fixture: ComponentFixture<StartNewInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartNewInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartNewInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
