import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartNewInvestmentComponent } from './start-new-investment.component';

const routes: Routes = [{ path: '', component: StartNewInvestmentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StartNewInvestmentRoutingModule { }
