import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StartNewInvestmentRoutingModule } from './start-new-investment-routing.module';
import { StartNewInvestmentComponent } from './start-new-investment.component';


@NgModule({
  declarations: [StartNewInvestmentComponent],
  imports: [
    CommonModule,
    StartNewInvestmentRoutingModule
  ]
})
export class StartNewInvestmentModule { }
