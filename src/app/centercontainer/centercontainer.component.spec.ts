import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentercontainerComponent } from './centercontainer.component';

describe('CentercontainerComponent', () => {
  let component: CentercontainerComponent;
  let fixture: ComponentFixture<CentercontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentercontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentercontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
