<<<<<<< HEAD
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomhttpService } from '../servives/customhttp.service';
import { CustomcacheService } from '../servives/customcache.service';
import { HttpErrorResponse } from '@angular/common/http';
=======
import { Component, OnInit } from '@angular/core';
>>>>>>> first commit from server

@Component({
  selector: 'app-banktransfer',
  templateUrl: './banktransfer.component.html',
  styleUrls: ['./banktransfer.component.scss']
})
export class BanktransferComponent implements OnInit {

<<<<<<< HEAD

  current_date: string;
  due_date: string;
  proof_uploaded: boolean = false;
  upload_failed: boolean = false;

  @ViewChild('bank_proof_payment', { static: true })
  private bank_proof_payment: HTMLImageElement;

  @ViewChild('image_file', { static: true })
  private imagefile: HTMLInputElement;

  constructor(
    private router: Router,
    private httpservice: CustomhttpService,
    private custom: CustomcacheService
  ) { }

  ngOnInit(): void {



    this.imagefile['nativeElement']['onchange'] = () => {
      this.previewFile(this.bank_proof_payment, this.imagefile);
    }


    let date = new Date(Date.now()),
      date2 = new Date(Date.now() + (30 * 24 * 3600 * 1000));
    this.current_date = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
    this.due_date = ((date2.getMonth() > 8) ? (date2.getMonth() + 1) : ('0' + (date2.getMonth() + 1))) + '/' + ((date2.getDate() > 9) ? date2.getDate() : ('0' + date2.getDate())) + '/' + date2.getFullYear()

  }



  /**
   * 
   * @param event Upload proof of payment to the API ser
   */
  uploadBankProof(event: Event) {
    this.proof_uploaded =  false ;
    event.preventDefault();
    this.httpservice.uploadBankProofOfPayment(sessionStorage.getItem('hash') , this.bank_proof_payment['nativeElement']['src']).subscribe(data => {
      this.proof_uploaded =  true ;
      //console.log(data);
      alert("Bank proof was uploaded successful. Please wait for the admin to confirm your payment.");
      this.router.navigate(['/tap-account-dashboard/investments/unapproved']); //navigate to unapproved invesment 
    }, (error: HttpErrorResponse) => {
      //console.log(error);
      this.proof_uploaded =  true ;
      alert("Error Occure" + error.message + " status ::: " + error.status)
    })
  }



  /**
   * 
   * @param image The Image HTML dom to display the input selected
   * @param inputFile The input file element from the DOM 
   */
  previewFile(image: HTMLImageElement, inputFile: HTMLInputElement) {
    const file = inputFile['nativeElement']['files'][0];
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      image['nativeElement']['src'] = reader.result;
      //console.log("Image Blob Format :::" + reader.result);
    }, false);
    if (file)  {
      reader.readAsDataURL(file);
    }
  }
=======
  constructor() { }

  ngOnInit(): void {
  }

>>>>>>> first commit from server
}
