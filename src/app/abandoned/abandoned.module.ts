import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbandonedRoutingModule } from './abandoned-routing.module';
import { AbandonedComponent } from './abandoned.component';


@NgModule({
  declarations: [AbandonedComponent],
  imports: [
    CommonModule,
    AbandonedRoutingModule
  ]
})
export class AbandonedModule { }
