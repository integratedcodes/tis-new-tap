import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AbandonedComponent } from './abandoned.component';

const routes: Routes = [{ path: '', component: AbandonedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbandonedRoutingModule { }
