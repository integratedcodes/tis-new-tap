import { Component, OnInit, ViewChild } from '@angular/core';
<<<<<<< HEAD
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomhttpService } from '../servives/customhttp.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomcacheService } from '../servives/customcache.service';
import { TapAgentAccountModel } from '../Model/agent-account-model';
// import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
=======
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomhttpService } from '../servives/customhttp.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
>>>>>>> first commit from server


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
<<<<<<< HEAD


  loginForm: FormGroup;
  showModal: boolean = false;
  showNotCorrect: boolean = false;
  loginInProcess : boolean = false ;
  connectionError : boolean =  false ;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: CustomhttpService,
    private cache: CustomcacheService,
    private userInformation :  TapAgentAccountModel
  ) { }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      username: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required),
    });

    this.loginForm.valueChanges.subscribe((data) => {
      //console.log(data);
    })

=======
  
  @ViewChild('errorLoginComponent') private deleteSwal: SwalComponent;
  @ViewChild('httpError') private httpError: SwalComponent;

  signInForm : FormGroup ;
  constructor(

    private router : ActivatedRoute,
    private fb : FormBuilder, 
    private http : CustomhttpService

  ) { 
    this.signInForm = this.fb.group({
      surname : new FormControl(""),
      firstname : new FormControl(""),
    })
  }

  ngOnInit(): void {
    // this.
>>>>>>> first commit from server
  }


  signIn() {
<<<<<<< HEAD

    this.loginInProcess =  true ;

    this.http.signIn(this.loginForm.value).subscribe((data) => {

      if (data['user']['is_verified'] == true) {

        this.cache.fullInformation = data['user'] ;

        // console.log(JSON.stringify(this.userInformation)  + " TAP LOGGED ");

        sessionStorage.setItem('user' , JSON.stringify(this.cache.fullInformation));

        sessionStorage.setItem('tap_data' , JSON.stringify(data));

        // console.log(sessionStorage.getItem('tap_data') + "WALE TOKEN");
        
        // console.log(data['user']);

        sessionStorage.setItem('loggedIn' , 'true') ;

        //console.log("login complete");
        //console.log(data) ;

        this.cache.access_token = data['access_token'];
        this.cache.token_type = data['token_type'];

        //console.log( "TOKEN_TYPE ::: " + this.cache.token_type ) ;
        //console.log( "ACCESS_TOKEN ::: " + this.cache.access_token ) ;

        //console.log(this.cache);


        this.cache.loggedIn = true ; 

        this.cache.username =  data['user']['username'];
        this.cache.full_name =  data['user']['surname']  + " " + data['user']['first_name'] + " " + data['user']['middle_name'];
        this.loginInProcess = false ;

        // alert("login was successful ");
        //view the successful login
        this.router.navigate(['/tap-account-dashboard/dashboard']);

      } else {
        this.loginInProcess = false ;
        this.showModal = true;
        sessionStorage.setItem('loggedIn' , 'false') ;
      }

    }, (error: HttpErrorResponse) => {
      this.loginInProcess = false ;
      if (error.status == 403) {
        //incorrect username and password
        //console.log(error)
        this.showNotCorrect = true;
        sessionStorage.setItem('loggedIn' , 'false') ;
      } else if (error.status == 405) {
        //account not verified 
        //console.log(error)
        this.showModal = true;
        sessionStorage.setItem('loggedIn' , 'false') ;
      }else {
        this.connectionError = true ;
        console.log(error);
      }
    });
    //console.log(this.loginForm.value);

  }

  close() {
    this.showModal = false;
  }


  closeNotCorrect() {
    this.showNotCorrect = false;
  }

  closeConnectionError() {
    this.connectionError = false ;
  }
=======
    //check if the account exist 
    //then login the account 
    this.http.signIn(this.signInForm.value).subscribe((data)=>{

      

      this.httpError.title = "Loging Was successful!";
      this.httpError.text = "Please wait while we take you to your account dashboard!";
      this.httpError.swalVisible = true ;



    }, (error : HttpErrorResponse)=> {
      console.log("Error Response : " + error.message);
      this.httpError.swalVisible = true ;
    });
  }

>>>>>>> first commit from server
}
