import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewInvestmentComponent } from './create-new-investment.component';

describe('CreateNewInvestmentComponent', () => {
  let component: CreateNewInvestmentComponent;
  let fixture: ComponentFixture<CreateNewInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNewInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
