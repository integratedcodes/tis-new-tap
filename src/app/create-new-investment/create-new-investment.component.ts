import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CustomcacheService } from '../servives/customcache.service';
import { CustomhttpService } from '../servives/customhttp.service';
import { Router } from '@angular/router';
import { error } from 'protractor';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-new-investment',
  templateUrl: './create-new-investment.component.html',
  styleUrls: ['./create-new-investment.component.scss']
})
export class CreateNewInvestmentComponent implements OnInit {


  investmentForm : FormGroup ;
  info : any  ;
  constructor(
    private fb  : FormBuilder ,
    private httpService : CustomhttpService ,
    private customCache : CustomcacheService,
    private router : Router
  ) { 
    this.info =  JSON.parse ( sessionStorage.getItem('tap_data') ) ;
    console.log(this.info);

    // console.log(this.info['user']['affiliate_information'] == null && this.info['user']['is_approved'] == 0) ;

    if(this.info['user']['affiliate_information'] == null || this.info['user']['is_approved'] == 0 ) {
      alert("Your account has not yet been approved ... \nYou can not make any invesmtent for now until your account is fully approved!");
      this.router.navigate(['/tap-account-dashboard/investments/start_new_investment']);
    } else {

      this.investmentForm = this.fb.group({
        affiliate_account_name : new FormControl( this.info['affiliate_information']['bank_account_name']  , Validators.required ),
        affiliate_account_number : new FormControl(  this.info['affiliate_information']['bank_account_number']  , Validators.required ),
        investment_duration : new FormControl(   "30 Days"  ,  [ Validators.required]),
        affiliate_bank_name: new FormControl(  this.info['affiliate_information']['bank_name']  , Validators.required ),
        amount : new FormControl("" ,  [ Validators.required , Validators.min(100_000) , Validators.max(100_000_000) ])
      })
  
      this.investmentForm.get('amount').valueChanges.subscribe(data=> { 
        if ( typeof data == 'string' && parseFloat(data) >  100000  ) {
          sessionStorage.setItem('amount' , data) ;
          //console.log("amount saved");
        }else {
          sessionStorage.setItem('amount' , '') ;
          //console.log("amount removed");
        }
      })
  
      //console.log(this.customCache)
      this.investmentForm.statusChanges.subscribe(()=> {
        for (let control in this.investmentForm.controls ) {
          // //console.log(control);
          //console.log ( control + "is ::: "  + this.investmentForm.get(control).status ) ;
        }
      })
    }
    
    
    // call validator 
  }


  ngOnInit(): void {

  }


  submitInvestmentWithCardPayment() {
    // this.httpService.
  }

  investmentWithBankTransferProofOfPayment() {

  }

  submitNewInvestment(event : Event ) {
    //console.log(this.investmentForm.value);
    event.preventDefault();
    //console.log(this.investmentForm.value);
    this.httpService.sendNewInvestment(this.investmentForm.value).subscribe (data=> {
      //console.log( " DAATA SENT ::: " + JSON.stringify(data));
      sessionStorage.setItem('hash' , data['data'][0]['investment_hash']);
      this.router.navigate(['/tap-account-dashboard/investments/start_new_investment/payment_methods']);
    } , (error :  HttpErrorResponse) => { 
      //console.log(error);
      new Promise ((resolve , reject)=>{
        resolve(alert("Error Occured while sending the new investment"))
      });
    })
  }


  

}
