import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateNewInvestmentRoutingModule } from './create-new-investment-routing.module';
import { CreateNewInvestmentComponent } from './create-new-investment.component';


@NgModule({
  declarations: [CreateNewInvestmentComponent],
  imports: [
    CommonModule,
    CreateNewInvestmentRoutingModule
  ]
})
export class CreateNewInvestmentModule { }
