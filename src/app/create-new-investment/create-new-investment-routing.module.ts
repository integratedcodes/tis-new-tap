import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateNewInvestmentComponent } from './create-new-investment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [{ path: '', component: CreateNewInvestmentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule , FormsModule , ReactiveFormsModule]
})
export class CreateNewInvestmentRoutingModule { }
