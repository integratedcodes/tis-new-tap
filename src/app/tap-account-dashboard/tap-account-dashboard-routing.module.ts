import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TapAccountDashboardComponent } from './tap-account-dashboard.component';
<<<<<<< HEAD
import { LoginguardGuard } from '../loginguard.guard';

const routes: Routes = [
  {


    path: '', component: TapAccountDashboardComponent, canActivate: [LoginguardGuard], redirectTo : "/dashboard",
    children: [
      { path: 'dashboard', loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule) },
      { path: 'affliate-program-application',
      loadChildren: () => import('../affliate-program-application/affliate-program-application.module')
      .then(m => m.AffliateProgramApplicationModule) },

      {
        path: 'account_settings', loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule),
        children: [
          { path: 'profile', loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule) }]
      },

      { path: 'fluterwavepay', loadChildren: () => import('../flutterwavepay/flutterwavepay.module').then(m => m.FlutterwavepayModule) },
      { path: 'banktransfer', loadChildren: () => import('../banktransfer/banktransfer.module').then(m => m.BanktransferModule) },

      {
        path: 'messages', loadChildren: () => import('../messages/messages.module').then(m => m.MessagesModule),
        children: [
          { path: 'received', loadChildren: () => import('../received/received.module').then(m => m.ReceivedModule) },
          { path: 'sent', loadChildren: () => import('../sent/sent.module').then(m => m.SentModule) },
          { path: 'compose', loadChildren: () => import('../compose/compose.module').then(m => m.ComposeModule) },
        ]
      },

      { path: 'investments', loadChildren: () => import('../investments/investments.module').then(m => m.InvestmentsModule) },
      { path: 'investments/completed', loadChildren: () => import('../completed/completed.module').then(m => m.CompletedModule) },
      { path: 'investments/abandoned', loadChildren: () => import('../abandoned/abandoned.module').then(m => m.AbandonedModule) },
      { path: 'investments/ongoing', loadChildren: () => import('../ongoing/ongoing.module').then(m => m.OngoingModule) },
      { path: 'investments/unapproved', loadChildren: () => import('../unapproved/unapproved.module').then(m => m.UnapprovedModule) },
      { path: 'investments/start_new_investment', loadChildren: () => import('../new-investment/new-investment.module').then(m => m.NewInvestmentModule) },
      { path: 'investments/start_new_investment/create_new_investment', loadChildren: () => import('../create-new-investment/create-new-investment.module').then(m => m.CreateNewInvestmentModule) },
      { path: 'investments/start_new_investment/payment_methods', loadChildren: () => import('../payment-methods/payment-methods.module').then(m => m.PaymentMethodsModule) },
      { path: 'investments/start_new_investment/payment_methods/pay_with_bank_transfer', loadChildren: () => import('../banktransfer/banktransfer.module').then(m => m.BanktransferModule) },

    ]
  },
=======

const routes: Routes = [
  {
    path: '', component: TapAccountDashboardComponent, children: [
      { path: 'dashboard', loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule) },
      { path: 'profile', loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule) },
      { path: 'settings', loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule) },
      { path: 'ongoing', loadChildren: () => import('../ongoing/ongoing.module').then(m => m.OngoingModule) },
      { path: 'investments', loadChildren: () => import('../investments/investments.module').then(m => m.InvestmentsModule) },
      { path: 'completed', loadChildren: () => import('../completed/completed.module').then(m => m.CompletedModule) },
      { path: 'abandoned', loadChildren: () => import('../abandoned/abandoned.module').then(m => m.AbandonedModule) },
      { path: 'forgotpassword', loadChildren: () => import('../forgotpassword/forgotpassword.module').then(m => m.ForgotpasswordModule) },
      { path: 'resetpassword', loadChildren: () => import('../resetpassword/resetpassword.module').then(m => m.ResetpasswordModule) },
      { path: 'fluterwavepay', loadChildren: () => import('../flutterwavepay/flutterwavepay.module').then(m => m.FlutterwavepayModule) },
      { path: 'banktransfer', loadChildren: () => import('../banktransfer/banktransfer.module').then(m => m.BanktransferModule) },
      { path: 'message', loadChildren: () => import('../messages/messages.module').then(m => m.MessagesModule) },
      { path: 'newInvestment', loadChildren: () => import('../new-investment/new-investment.module').then(m => m.NewInvestmentModule) },

    ]
  }
>>>>>>> first commit from server
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TapAccountDashboardRoutingModule { }
