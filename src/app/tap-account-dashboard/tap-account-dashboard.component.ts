import { Component, OnInit, ViewContainerRef, ViewChild, Injector, ComponentFactoryResolver } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tap-account-dashboard',
  templateUrl: './tap-account-dashboard.component.html',
  styleUrls: ['./tap-account-dashboard.component.scss']
})
export class TapAccountDashboardComponent implements OnInit {
  @ViewChild('leftsidenav', { read: ViewContainerRef, static: true })
  private leftsidenav: ViewContainerRef;

  @ViewChild('rightsidenav', { read: ViewContainerRef, static: true })
  private rightsidenav: ViewContainerRef;

  @ViewChild('header', { read: ViewContainerRef, static: true })
  private topheader: ViewContainerRef;


  @ViewChild('footer', { read: ViewContainerRef, static: true })
  private footercontainer: ViewContainerRef;


  showP: boolean;
  investors: Array<Object> = [{ 'name': "", "date": "", amount: '', completed: ' ' }]
  constructor(private meta: Meta, private title: Title,
    private inj: Injector, private resolser: ComponentFactoryResolver) { }

  async ngOnInit() {

    this.title.setTitle("Account Dashboard | Affliate Account Dashboard ");

    const { SidenavComponent } = await import('../sidenav/sidenav.component');
    const sideNaveFactory = this.resolser.resolveComponentFactory(SidenavComponent);
    this.leftsidenav.createComponent(sideNaveFactory, null, this.inj);

    const { RightnavComponent } = await import('../rightnav/rightnav.component');
    const rightsidenavFactory = this.resolser.resolveComponentFactory(RightnavComponent);
    this.rightsidenav.createComponent(rightsidenavFactory, null, this.inj);

    const { DashboardHeaderComponent } = await import('../dashboard-header/dashboard-header.component');
    const dashboardHeaderFactory = this.resolser.resolveComponentFactory(DashboardHeaderComponent);
    this.topheader.createComponent(dashboardHeaderFactory, null, this.inj);


   


    const { FooterComponent } = await import('../footer/footer.component');
    const footerFactory = this.resolser.resolveComponentFactory(FooterComponent);
    this.footercontainer.createComponent(footerFactory, null, this.inj);


  }


  prevent(event: Event) {
    event.preventDefault();
  }
}
