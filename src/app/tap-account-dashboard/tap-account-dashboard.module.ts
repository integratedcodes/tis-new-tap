import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TapAccountDashboardRoutingModule } from './tap-account-dashboard-routing.module';
import { TapAccountDashboardComponent } from './tap-account-dashboard.component';


@NgModule({
  declarations: [TapAccountDashboardComponent],
  imports: [
    CommonModule,
    TapAccountDashboardRoutingModule
  ]
})
export class TapAccountDashboardModule { }
