import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TapAccountDashboardComponent } from './tap-account-dashboard.component';

describe('TapAccountDashboardComponent', () => {
  let component: TapAccountDashboardComponent;
  let fixture: ComponentFixture<TapAccountDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapAccountDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapAccountDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
