import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewInvestmentRoutingModule } from './new-investment-routing.module';
import { NewInvestmentComponent } from './new-investment.component';


@NgModule({
  declarations: [NewInvestmentComponent],
  imports: [
    CommonModule,
    NewInvestmentRoutingModule
  ]
})
export class NewInvestmentModule { }
