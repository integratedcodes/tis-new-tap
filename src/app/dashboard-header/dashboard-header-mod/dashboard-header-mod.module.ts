import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardHeaderModRoutingModule } from './dashboard-header-mod-routing.module';
import { DashboardHeaderComponent } from '../dashboard-header.component';


@NgModule({
  declarations: [DashboardHeaderComponent],
  imports: [
    CommonModule,
    DashboardHeaderModRoutingModule 
  ],
  schemas : [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class DashboardHeaderModModule { }
