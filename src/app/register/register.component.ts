import { Component, OnInit } from '@angular/core';
<<<<<<< HEAD
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, FormsModule } from '@angular/forms';
import { CustomhttpService } from '../servives/customhttp.service';
import { HttpErrorResponse } from '@angular/common/http';
=======
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, FormsModule } from '@angular/forms';
>>>>>>> first commit from server

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

<<<<<<< HEAD
  regForm: FormGroup;
  iscomplete: boolean;
  showForm: boolean = true;
  showTerms: boolean = false;
  showSuccessful: boolean = false;
  regprogress: boolean = false;
  agreed: boolean = false
  same: boolean = false;
  username_exist: boolean = false;
  email_exist: boolean = false;
  error: boolean = false;
  error_message : string  =  "";
  error_detail : string = "";

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private customHttp: CustomhttpService,
  ) {
    this.regForm = this.fb.group({
      surname: new FormControl("", Validators.required),
      first_name: new FormControl("", Validators.required),
      middle_name: new FormControl("", Validators.required),
      sex: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.email, Validators.required]),
      username: new FormControl("", Validators.required),
      dob: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required),
      cpassword: new FormControl("", Validators.required),
      phone_number: new FormControl("", [Validators.required]),
      agree: new FormControl(false, Validators.required)
    })

    this.regForm.get('username').valueChanges.subscribe(data => {
      this.checkUsername();
    })

    this.regForm.get('email').valueChanges.subscribe(data => {
      this.checkEmail();
    })

    this.regForm.statusChanges.subscribe(data => {
      this.findInvalidControls();
    });


    let pass = "", cpass = "";
    this.regForm.valueChanges.subscribe(data => {
      pass = this.regForm.get('password').value;
      cpass = this.regForm.get('cpassword').value;
      ////console.log( "PASSWORD :::: " + pass + "cpass ::: " + cpass)
      if (pass === cpass) {
        this.same = true;
      } else {
        this.same = false;
      }
    });
=======
  regForm : FormGroup ;
  constructor(
    private router : ActivatedRoute,
    private fb : FormBuilder 
  ) { 
    this.regForm = this.fb.group({
      surname : new FormControl("" , Validators.required),
      firstname : new FormControl("" , Validators.required),
      middlename : new FormControl("" , Validators.required ),
      sex : new FormControl("" , [Validators.required]),
      email : new FormControl("", [Validators.email , Validators.required]),
      username : new FormControl("", Validators.required),
      dob : new FormControl("", Validators.required),
      password : new FormControl("", Validators.required),
      cpassword : new FormControl("", Validators.required),
      phoneNumber : new FormControl("" , Validators.required)
    })
>>>>>>> first commit from server
  }

  ngOnInit(): void {
  }

<<<<<<< HEAD
  regComplete() {

    ////console.log(this.regForm.get('agree'));

    // if (this.regForm.valid == false ) {
    //   alert("please check all fields and make sure you agree to terms and condition");
    //    ////console.log(this.regForm.value);
    // }else {

    this.regprogress = true;
    this.iscomplete = false;
    const payload = this.regForm.value;
    this.showForm = false;

    delete this.regForm.value['cpassword'];
    delete this.regForm.value['agree'];

    this.customHttp.createNewUser(payload).subscribe((data) => {

      ////console.log("Response ::::: " + JSON.stringify(data) );
      this.showSuccessful = true;
      this.regprogress = false;

      // alert("Registration was successful , please kindly check your email to activate your account.");
      this.iscomplete = true;
      this.showForm = true;
    }, (error: HttpErrorResponse) => {
      ////console.log(error);
      this.showForm = true;
      this.regprogress = false;
      if (error.status == 400) {
        this.error = true;
        this.error_message = "Registration not Complete";
        this.error_detail = "In the process of registering your account, we ecountered an exception. please try back later!";
        // alert("Please make sure all fields are checked. Failed to submit resonses. ERROR_CODE ::: 400 ")
      } else if (error.status == 403 ) {
        this.error = true;
        this.error_message = "Registration not Complete";
        this.error_detail = "Registration is rejected.... \nWe are so sorry to announce to you that the TAP agents slot is filled up! \nPlease register later. Thank you!";
      } else {
        this.error = true;
        // alert("Error Occured  " + error.message + "\nERROR_CODE : " + error.status + "\nStatus text : " + error.statusText);
        this.error_message = "Registration not Complete";
        this.error_detail = "Registration could not be completed .... ERROR_CODE ::: " + error.status;
        // alert("Registration could not be completed .... ERROR_CODE ::: " + error.status)
      }
    });
    ////console.log(this.regForm.value);

    // }


  }


  private checkUsername() {
    ////console.log("checking if username already exist");
    this.customHttp.checkUserExist(this.regForm.get('username').value).subscribe(data => {
      ////console.log(data);
      this.username_exist = false;
    }, (error: HttpErrorResponse) => {
      if (error.status == 403) {
        this.username_exist = true;
        //console.log(error);
      } else {
        this.username_exist = false;
        //console.log("Username taken" + this.username_exist)
        ////console.log(error);
      } ////console.log(error);
    });
  }


  private checkEmail() {
    ////console.log("checking if username already exist");
    this.customHttp.checkEmailExist(this.regForm.get('email').value).subscribe(data => {
      //console.log(data);
      this.email_exist = false;
    }, (error: HttpErrorResponse) => {
      if (error.status == 403) {
        this.email_exist = true;
        //console.log("Email taken" + this.email_exist)
        //console.log(error);
      } else {
        this.email_exist = false;
        //console.log(error);
      } ////console.log(error);
    });
  }


  close() {
    this.showTerms = false;
  }

  showterms() {
    this.showTerms = true;
  }

  login() {
    this.router.navigate(["/login"]);
  }


  findInvalidControls() {
    if (this.regForm.get('agree').value == true) {
      this.agreed = true;
    } else {
      this.agreed = false;
    }
    const controls = this.regForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        ////console.log(name);
        continue;
      } else {
        ////console.log(name) ;
        return true;
      }
    }
  }

  closeError() {
    this.error = false;
  }
=======
  regComplete(){
    // navigate to the registration complete page 
    console.log(this.regForm.value);
  }

>>>>>>> first commit from server
}
