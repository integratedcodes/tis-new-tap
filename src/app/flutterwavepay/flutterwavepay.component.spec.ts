import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlutterwavepayComponent } from './flutterwavepay.component';

describe('FlutterwavepayComponent', () => {
  let component: FlutterwavepayComponent;
  let fixture: ComponentFixture<FlutterwavepayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlutterwavepayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlutterwavepayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
