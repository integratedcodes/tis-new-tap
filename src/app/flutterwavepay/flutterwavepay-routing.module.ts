import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlutterwavepayComponent } from './flutterwavepay.component';

const routes: Routes = [{ path: '', component: FlutterwavepayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlutterwavepayRoutingModule { }
