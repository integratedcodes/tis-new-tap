import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnapprovedComponent } from './unapproved.component';

const routes: Routes = [{ path: '', component: UnapprovedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnapprovedRoutingModule { }
