import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnapprovedRoutingModule } from './unapproved-routing.module';
import { UnapprovedComponent } from './unapproved.component';


@NgModule({
  declarations: [UnapprovedComponent],
  imports: [
    CommonModule,
    UnapprovedRoutingModule
  ]
})
export class UnapprovedModule { }
