import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountVerifyWithTokenRoutingModule } from './account-verify-with-token-routing.module';
import { AccountVerifyWithTokenComponent } from './account-verify-with-token.component';


@NgModule({
  declarations: [AccountVerifyWithTokenComponent],
  imports: [
    CommonModule,
    AccountVerifyWithTokenRoutingModule
  ]
})
export class AccountVerifyWithTokenModule { }
