import { Component, OnInit } from '@angular/core';
import { CustomhttpService } from '../servives/customhttp.service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { error } from 'protractor';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-account-verify-with-token',
  templateUrl: './account-verify-with-token.component.html',
  styleUrls: ['./account-verify-with-token.component.scss']
})
export class AccountVerifyWithTokenComponent implements OnInit {

  showVerified : boolean = false ;
  showError : boolean = false ;

  constructor(
    private customHttp : CustomhttpService,
    private router : Router,
    private activatedRoute : ActivatedRoute
  ) { 
  let token = "" ;
   this.activatedRoute.queryParamMap.subscribe((data)=> {
    token = data['token'] ;
    this.verifyToken(data.get('token'));
    //console.log(data.get("token"));
   });
   //console.log(token);
  }

  ngOnInit(): void {

  }



  /**
   * 
   * @param token the token to be verified on the server
   */
  verifyToken(token  : string ) {
    this.customHttp.verifyAccountActivation(token).subscribe((data)=> {
      //console.log("DATA ::::: " + JSON.stringify(data));
      if (data['status']== "success") {
        this.showVerified =  true ;
      }else {
        this.showError =  true ;
      }
    },(error : HttpErrorResponse)=> {
      this.showError  = true ;
      //console.log(error);
      // alert("Error Occured with status ::: "+ error.status + "\nMessage : " + error.message)
    })
  }

  goback() {
    this.router.navigate(['/login']);
  }

}
