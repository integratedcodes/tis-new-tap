import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountVerifyWithTokenComponent } from './account-verify-with-token.component';

describe('AccountVerifyWithTokenComponent', () => {
  let component: AccountVerifyWithTokenComponent;
  let fixture: ComponentFixture<AccountVerifyWithTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountVerifyWithTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountVerifyWithTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
