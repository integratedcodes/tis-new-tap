import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountVerifyWithTokenComponent } from './account-verify-with-token.component';

const routes: Routes = [{ path: '', component: AccountVerifyWithTokenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountVerifyWithTokenRoutingModule { }
