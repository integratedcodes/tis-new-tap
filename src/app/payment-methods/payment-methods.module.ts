import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentMethodsRoutingModule } from './payment-methods-routing.module';
import { PaymentMethodsComponent } from './payment-methods.component';
import { AngularRaveModule } from 'angular-rave';


@NgModule({
  declarations: [PaymentMethodsComponent],
  imports: [
    CommonModule,
    PaymentMethodsRoutingModule,
    AngularRaveModule.forRoot({
      key: 'FLWPUBK-1000',
      isTest: true,
    }),
  ]
})
export class PaymentMethodsModule { }
