import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomhttpService } from '../servives/customhttp.service';
import { PaymentInstance } from 'angular-rave';


@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.component.html',
  styleUrls: ['./payment-methods.component.scss']
})
export class PaymentMethodsComponent implements OnInit {

  current_date: string;
  due_date: string;
  full_name : string ;
  user : any ;
  amount_to_pay: number  ;
  activate_payment : boolean = false ;



  paymentInstance: PaymentInstance;
  token :string

  constructor(
    private router : Router ,
    private customHttp : CustomhttpService
  ) { }

  ngOnInit(): void {

    this.user = JSON.parse(sessionStorage.getItem('user'));
    this.full_name =  this.user['surname'] +  " " + this.user['first_name'] + " " + this.user['middle_name'] ;

    let amount  =  sessionStorage.getItem('amount') ;
    ////console.log(sessionStorage.getItem('amount'));
    if ( amount !="" || amount!=undefined) {
      this.activate_payment = true ;
      this.amount_to_pay = parseFloat(amount) ;
    }
  


    let date = new Date ( Date.now() ) ,
    date2 = new Date( Date.now() + ( 30 * 24 * 3600 * 1000 ) );
    this.current_date =     ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
    this.due_date =     ((date2.getMonth() > 8) ? (date2.getMonth() + 1) : ('0' + (date2.getMonth() + 1))) + '/' + ((date2.getDate() > 9) ? date2.getDate() : ('0' + date2.getDate())) + '/' + date2.getFullYear()



  }



  paymentFailure() {
    ////console.log('Payment Failed');
  }
 
  paymentSuccess(res) {
    ////console.log('Payment complete', res);
    this.paymentInstance.close();
  }
 
  paymentInit() {
    // this.paymentFailure = paymentInstance;
    ////console.log('Payment about to begin', this.paymentInstance);
  }


  payWithFlutterwave() {

  }

  verifyPaymentFromServer() {

  }

  payWithBank() {
    this.router.navigate(['/tap-account-dashboard/investments/start_new_investment/payment_methods/pay_with_bank_transfer' ])
  }

}
