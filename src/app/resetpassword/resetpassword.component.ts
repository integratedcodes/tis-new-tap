import { Component, OnInit } from '@angular/core';
<<<<<<< HEAD
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomhttpService } from '../servives/customhttp.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
=======
>>>>>>> first commit from server

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

<<<<<<< HEAD
  resetPassword: FormGroup;
  same: boolean = false;
  errorMessage: string = "";
  success: boolean = false;
  eror: boolean = false;
  tokenError: boolean = false;
  error_message : string =  "";
  details : string  = "";
  error : boolean =  false ;
  token_provided : string =  "";
  loading : boolean = false ;

  constructor(
    private fb: FormBuilder,
    private http: CustomhttpService,
    private router: Router ,
    private activatedRoute : ActivatedRoute
  ) {

    this.resetPassword = this.fb.group({
      password: new FormControl("", Validators.required),
      cpassword: new FormControl("", [Validators.required])
    });

  }
   

  ngOnInit() : void {
    
    let pass = "", cpass = "";
    this.resetPassword.valueChanges.subscribe(data => {
      pass = this.resetPassword.get('password').value;
      cpass = this.resetPassword.get('cpassword').value;
      if (pass === cpass) {
        this.same = true;
      } else {
        this.same = false;
      }
    });

    this.activatedRoute.queryParamMap.subscribe((data) => {
      this.token_provided =  data.get('token');
      ////console.log(data.get("token"));
    });
    // ////console.log(token)
  }


  /**
   * 
   * @param token the token to be verified on the server
   */
  updatepassword(event : Event) {
    event.preventDefault();

    this.loading = true;
    this.http.resetPasswordWithToken( this.token_provided  , this.resetPassword.value ).subscribe((data) => {
      ////console.log("DATA ::::: " + JSON.stringify(data));
        this.success = true;
        this.loading =  false ;
    
    }, (error: HttpErrorResponse) => {
      console.log(error);
      if ( error.status == 400 ) {
        this.loading =  false ;
        this.error = true ;
        this.error_message = "Error Occured";
        this.details =  "The URL is not valid or the reset password token is not valid anymore.Please request for another link";
        setTimeout(() => {
          this.router.navigate(['/forgotpassword']);
        }, 1500);
      } else {
        this.error = true ;
        this.loading = false ;
        this.error_message = "Error Occured";
        this.details = "Could not valide the token due to an error with code :: " + error.status;
      }
     
    })
  }

  go2ForgotPassword() {
    this.router.navigate(['/forgotpassword']);
  }
  
  close() {
    this.success  = false ;
    this.router.navigate(['/login']);
  }

  closeError() {
    this.error =  false ;
  }


  // updatePassword(event : Event) {
  //   event.preventDefault();
  //   this.http.resetpassword(this.resetPassword.value).subscribe((data) => {
  //     if (data['status'] == "success") {
  //       this.success = true;
  //     } else {
  //       this.eror = true;
  //     }
  //   }, (error: HttpErrorResponse) => {
  //     this.errorMessage =  "Error Occured";
  //     this.details = "Failed to reset account password , please try again or request for another forgot password link!";
  //     this.eror = true;
  //   })
  // }

=======
  constructor() { }

  ngOnInit(): void {
  }

>>>>>>> first commit from server
}
