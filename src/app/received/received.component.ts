import { Component, OnInit } from '@angular/core';
import { CustomhttpService } from '../servives/customhttp.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-received',
  templateUrl: './received.component.html',
  styleUrls: ['./received.component.scss']
})
export class ReceivedComponent implements OnInit {

  messages = [{}] ;
  inbox_count : number  = 0 ;
  sent_message_count : number = 0 ;
  title : string = "";
  body : string = ""; 

  constructor(
    private http : CustomhttpService
  ) {}

  ngOnInit() {
    // this.messages = [{'date' : 'February 22 , 2020 .' , 'text' : 'Welcome to tap' , 'sender' : 'admin'}];
    this.getReceivedMessages();
  }


  getReceivedMessages() {
    this.http.receivedMessages().subscribe(data=> {
      //console.log(data);
      //console.log(data['data']['messages'][0]['title'])
      //console.log(data['data']['messages'][0]['body'])
      this.messages = data['data']['messages'];
      this.inbox_count = this.messages.length ;
    } , (error : HttpErrorResponse)=> {
      //console.log(error);
      alert("ERROR OCCURED  :::: " +  error.message);
    })
  }


  displayMessage(event : Event ,title : string , body : string) {
    event.preventDefault();
    this.title =  title ;
    this.body =  body ;
  }

}
