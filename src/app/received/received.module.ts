import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceivedRoutingModule } from './received-routing.module';
import { ReceivedComponent } from './received.component';


@NgModule({
  declarations: [ReceivedComponent],
  imports: [
    CommonModule,
    ReceivedRoutingModule
  ]
})
export class ReceivedModule { }
