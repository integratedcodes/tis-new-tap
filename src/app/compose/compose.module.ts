import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComposeRoutingModule } from './compose-routing.module';
import { ComposeComponent } from './compose.component';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ComposeComponent],
  imports: [
    CommonModule,
    ComposeRoutingModule,
    FormsModule ,
    ReactiveFormsModule
  ]
})
export class ComposeModule { }
