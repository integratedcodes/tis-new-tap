import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CustomhttpService } from '../servives/customhttp.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.scss']
})
export class ComposeComponent implements OnInit {

  composedMessage : FormGroup ;
  showButton : boolean  = true ;
  constructor(
    private fb : FormBuilder,
    private httpService : CustomhttpService
  ) { 
    this.composedMessage = this.fb.group({
      title : new FormControl("" , Validators.required),
      message : new FormControl("" , Validators.required)
    })

    // this.composedMessage.statusChanges.subscribe(data=> {
    //   this.showButton = data;
    // })
  }

  ngOnInit(): void {
  }


  public sendMessage() {
    this.showButton =  false ;
    ////console.log(this.composedMessage.value);
    this.httpService.sendMessageToAdmin(this.composedMessage.value).subscribe(
      data => {
        ////console.log(data);
        alert(" Message was successfully sent ");
        this.showButton = true ;
      },
      (error : HttpErrorResponse)=> {
        this.showButton = true ;
        //console.log(error);
        alert("ERROR OCCURED ::: " + error.message + " STATUS ::: " + error.status)
      }
    )
  }

}
