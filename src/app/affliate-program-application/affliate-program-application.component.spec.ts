import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffliateProgramApplicationComponent } from './affliate-program-application.component';

describe('AffliateProgramApplicationComponent', () => {
  let component: AffliateProgramApplicationComponent;
  let fixture: ComponentFixture<AffliateProgramApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffliateProgramApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffliateProgramApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
