import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AffliateProgramApplicationRoutingModule } from './affliate-program-application-routing.module';
import { AffliateProgramApplicationComponent } from './affliate-program-application.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TapAgentAccountModel } from '../Model/agent-account-model';


@NgModule({
  declarations: [AffliateProgramApplicationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AffliateProgramApplicationRoutingModule
  ],
  providers : [TapAgentAccountModel]
})
export class AffliateProgramApplicationModule { }
