import { Component, OnInit, ViewChild } from "@angular/core";
import { CustomcacheService } from "../servives/customcache.service";
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup
} from "@angular/forms";
import { TapAgentAccountModel } from "../Model/agent-account-model";
import { Router } from "@angular/router";
import { CustomhttpService } from "../servives/customhttp.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "app-affliate-program-application",
  templateUrl: "./affliate-program-application.component.html",
  styleUrls: ["./affliate-program-application.component.scss"]
})
export class AffliateProgramApplicationComponent implements OnInit {
  @ViewChild("profilePicture", { static: true })
  private profileImage: HTMLImageElement;

  @ViewChild("idcard_preview", { static: true })
  private idcard_preview: HTMLImageElement;

  // @ViewChild('bank_statement_preview' , {static : true})
  // private bank_statement_preview : HTMLImageElement ;

  user: any;
  message: string =
    "Sending Details your information ... Stay on this page ....";
  errorMessage: string = "";

  @ViewChild("imagefile", { static: true })
  private imagefile: HTMLInputElement;

  @ViewChild("idcard", { static: true })
  private idcardfile: HTMLInputElement;

  @ViewChild("bank_statement", { static: true })
  private bankstatementfile: HTMLInputElement;

  affliateAccountVerification: FormGroup;

  account_verified: boolean = false;
  hideOverlayBool: false; // hide the overlay
  showSweetAlertSuccess: boolean = false;
  showError: boolean = false;
  showSuccess: boolean = false;
  is_approved: boolean = false;
  has_uploaded: boolean = false;
  showSending: boolean = false;
  profile: TapAgentAccountModel;

  account_not_verified: boolean = true;
  constructor(
    private cache: CustomcacheService,
    private fb: FormBuilder,
    private router: Router,
    private customHttp: CustomhttpService
  ) {
    this.user = JSON.parse(sessionStorage.getItem("user"));
    this.affliateAccountVerification = this.fb.group({
      first_name: new FormControl(
        { value: this.user["first_name"], disabled: true },
        [Validators.required]
      ),
      surname: new FormControl(
        { value: this.user["surname"], disabled: true },
        Validators.required
      ),
      middle_name: new FormControl(
        { value: this.user["middle_name"], disabled: true },
        Validators.required
      ),
      sex: new FormControl(
        { value: this.user["sex"], disabled: true },
        Validators.required
      ),
      address: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      state: new FormControl("", Validators.required),
      country: new FormControl("", Validators.required),
      postal_code: new FormControl("", Validators.required),
      phone_number: new FormControl(
        this.user["phone_number"],
        Validators.required
      ),
      occupation: new FormControl("", Validators.required),
      dob: new FormControl(this.user["dob"], Validators.required),
      email: new FormControl(
        { value: this.user["email"], disabled: true },
        Validators.required
      ),
      username: new FormControl(
        { value: this.user["username"], disabled: true },
        Validators.required
      ),
      bank_statement_pdf: new FormControl("", Validators.required),
      profile_img: new FormControl("", Validators.required),
      idcard_img: new FormControl("", Validators.required),
      bank_name: new FormControl("", Validators.required),
      bank_account_number: new FormControl("", Validators.required),
      bank_account_name: new FormControl("", Validators.required),
      idcard_type: new FormControl("", Validators.required)
    });
  }

  ngOnInit(): void {
    //console.log(this.imagefile);

    //console.log(this.cache.fullInformation);
    this.is_approved = this.user["is_approved"];
    this.has_uploaded = this.user["has_uploaded"];

    if (this.cache.account_verified) {
      this.account_verified = true;
    } else {
      this.account_not_verified = false;
      // //console.log(this.profileImage['nativeElement']['src']);
      //console.log(this.imagefile);

      this.imagefile["nativeElement"]["onchange"] = () => {
        this.previewFile(
          this.profileImage,
          this.imagefile,
          this.affliateAccountVerification,
          "profile_img"
        );
      };

      this.bankstatementfile["nativeElement"]["onchange"] = () => {
        //console.log('previewing');
        this.previewFile(
          null,
          this.bankstatementfile,
          this.affliateAccountVerification,
          "bank_statement_pdf"
        );
      };

      this.idcardfile["nativeElement"]["onchange"] = () => {
        this.previewFile(
          this.idcard_preview,
          this.idcardfile,
          this.affliateAccountVerification,
          "idcard_img"
        );
      };
    }
  }

  convertBase64ToBlob_() {
    const b64toBlob = (b64Data, contentType = "", sliceSize = 512) => {
      const byteCharacters = atob(b64Data);

      const byteArrays = [];

      for (
        let offset = 0;
        offset < byteCharacters.length;
        offset += sliceSize
      ) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);

        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      const blob = new Blob(byteArrays, { type: contentType });

      return blob;
    };
  }

  /**
   *
   * @param event The event from the template being binded to the event
   */
  submitDetails(event: Event): void {
    event.preventDefault();
    this.showSending = true;

    //console.log(this.affliateAccountVerification.value);

    this.customHttp
      .submitDetails(this.affliateAccountVerification.value)
      .subscribe(
        data => {
          //console.log(data);
          this.showSending = false;
          this.showSuccess = true;
        },
        (error: HttpErrorResponse) => {
          //console.log(error);

          if (error.status === 401) {
            this.message =
              "Retrying to send your application... please stay on this page!";
            this.showError = false;
            this.showSending = true;

            this.submitDetails(event);
          } else if (error.status === 500) {
            this.errorMessage = "Server Error encountered... please try again!";
            this.showError = true;
            this.showSending = false;
          } else {
            this.errorMessage =
              "Failed to send your application....ERROR_CODE ::: " +
              error.status;
            this.showError = true;
            this.showSending = false;
          }
        }
      );
  }

  previewFile(
    image: HTMLImageElement,
    inputFile: HTMLInputElement,
    formGroup: FormGroup,
    fromControlName: string
  ) {
    const file = inputFile["nativeElement"]["files"][0];
    const reader = new FileReader();
    let value;

    //once the data is beign loaded
    reader.addEventListener(
      "load",
      () => {
        // convert image file to base64 string

        if (fromControlName !== "bank_statement_pdf") {
          image["nativeElement"]["src"] = reader.result;
        }

        formGroup.get(fromControlName).setValue(reader.result);

        //console.log('Image Blob Format :::' + reader.result);
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  /**
   *
   * @param base64ImageString The base 64 string to convert to blob
   */
  convertBase64ToBlob(base64ImageString: string) {
    return atob(base64ImageString);
  }

  closeSuccess() {
    this.showSuccess = true;
    this.router.navigate(["/tap-account-dashboard/dashboard"]);
  }

  closeError() {
    this.showError = false;
  }
}
