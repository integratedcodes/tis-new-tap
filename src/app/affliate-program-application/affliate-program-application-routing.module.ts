import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AffliateProgramApplicationComponent } from './affliate-program-application.component';

const routes: Routes = [{ path: '', component: AffliateProgramApplicationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AffliateProgramApplicationRoutingModule { }
