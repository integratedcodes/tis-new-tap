<<<<<<< HEAD
import { Component, OnInit, ComponentFactoryResolver, Injector, ViewChild, ViewContainerRef } from '@angular/core';
import { FormGroupDirective, FormGroup, FormControl, FormBuilder, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { CustomcacheService } from '../servives/customcache.service';
import { CustomhttpService } from '../servives/customhttp.service';
import { error } from 'protractor';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
=======
import { Component, OnInit } from '@angular/core';
>>>>>>> first commit from server

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

<<<<<<< HEAD
  user: any;
  updateError: boolean;
  updateSuccess: boolean;
  upInPorgress: boolean = false;
  successMessage: string = "";
  errorMessage: string = "";
  updateDetails: string;
  updateMessage: string;
  imageBlob: any;
  full_name: string;


  @ViewChild('profile_picture', { static: true })
  private profileImage: HTMLImageElement;


  @ViewChild('profile_img', { read: ViewContainerRef, static: true })
  private profile_img: ViewContainerRef;

  @ViewChild('image_file', { static: true })
  private imagefile: HTMLInputElement;


  profile: FormGroup;
  username: FormGroup;
  password: FormGroup;
  alreadyExists: boolean = false;
  same: boolean = false;
  profilePicture: string = "";
  delete_account: boolean = false;
  showDeleteButton: boolean = false;
  showLoader: boolean = true;
  deleteMessage: boolean = true;
  success : boolean =  false ;


  constructor(
    private cache: CustomcacheService, private fb: FormBuilder,
    private http: CustomhttpService,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private router: Router
  ) {



    ////console.log("CACHE" + JSON.stringify(this.cache));
    this.user = JSON.parse(sessionStorage.getItem('user'));
    this.full_name = this.user['surname'] + " " + this.user['first_name'] + " " + this.user['middle_name'];


    this.profile = this.fb.group({

      first_name: new FormControl({ value: this.user['first_name'], disabled: false }, [Validators.required]),
      surname: new FormControl({ value: this.user['surname'], disabled: false }, Validators.required),
      middle_name: new FormControl({ value: this.user['middle_name'], disabled: false }, Validators.required),
      sex: new FormControl({ value: this.user['sex'], disabled: false }, Validators.required),
      address: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      state: new FormControl("", Validators.required),
      country: new FormControl("", Validators.required),
      postal_code: new FormControl("", Validators.required),
      phone_number: new FormControl(this.user['phone_number'], Validators.required),
      occupation: new FormControl("", Validators.required),
      dob: new FormControl(this.user['dob'], Validators.required),
      email: new FormControl({ value: this.user['email'], disabled: false }, Validators.required),
      username: new FormControl({ value: this.user['username'], disabled: false }, Validators.required),
      bank_name: new FormControl("", Validators.required),
      bank_account_number: new FormControl("", Validators.required),
      profile_picture: new FormControl(this.user['profile_picture'])

    });



    this.password = this.fb.group({
      password: new FormControl("", Validators.required),
      cpassword: new FormControl("", [Validators.required])
    });


    let pass = "", cpass = "";
    this.password.valueChanges.subscribe(data => {
      pass = this.password.get('password').value;
      cpass = this.password.get('cpassword').value;
      ////console.log("PASSWORD :::: " + pass + "cpass ::: " + cpass)
      if (pass === cpass) {
        // this.password.get('cpassword').setErrors({'same' : false });
        // ////console.log ( "HAS ERROR  :: " + this.password.get('cpassword').getError('same') );
        // ////console.log(this.password.invalid);
        this.same = true;
      } else {
        this.same = false;
        // this.password.get('cpassword').setErrors({'same' : true });
      }
    });




    this.username = this.fb.group({
      username: new FormControl("", Validators.required),
    });

    this.profile.get('username').valueChanges.subscribe(data => {
      this.checkUsername();
    })






  }


  private checkUsername() {
    ////console.log("checking if username already exist");
    this.http.checkUserExist(this.username.value).subscribe(data => {
      ////console.log(data);
      this.alreadyExists = false;
    }, (error: HttpErrorResponse) => {
      if (error.status == 403) {
        this.alreadyExists = true;
        ////console.log(error);
      } else {
        this.alreadyExists = true;
        ////console.log(error);
      } ////console.log(error);
    });
  }


  async ngOnInit() {


    let user = JSON.parse(sessionStorage.getItem('user'));
    if (JSON.parse(sessionStorage.getItem('tap_data'))['affiliate_profile'] == undefined) {
      this.profilePicture = '../assets/images/user3.png';
    } else {
      this.profilePicture = JSON.parse(sessionStorage.getItem('tap_data'))['affiliate_profile']['profile_img'];
      this.username = user['username'];
      this.full_name = user['surname'] + " " + user['first_name'] + " " + user['middle_name'];
    }

    this.imagefile['nativeElement']['onchange'] = () => {
      this.previewFile(this.profileImage, this.imagefile);
    }





  }


  updateProfile(event: Event) {
    // this.profile.removeControl('profile_picture'); // remove the profile picture before sending 
    this.upInPorgress = true;
    this.updateMessage = 'Updating your Profile';
    this.updateDetails = "...please wait while we update your profile information!"
    event.preventDefault();
    this.upInPorgress = true;

    this.http.updateProfile(this.profile.value).subscribe(data => {
      this.deleteMessage = false;
      this.success = true ;
      this.successMessage = "Profile was successfully updated....";
      this.upInPorgress = false;
      this.updateSuccess = true;
      this.router.navigate(['/tap-account-dashboard/account_settings/profile']);
      this.profile.addControl("profile_picture", new FormControl("")); // remove the profile picture before sending 

    }, (error: HttpErrorResponse) => {
      this.upInPorgress = false;
      ////console.log(error);
      this.errorMessage = "Error Occured while trying to update profile information"
      this.updateError = true;
    })
  }


  closeUpdateError() {
    this.updateError = false;
  }

  closeUpdateSuccess() {
    this.updateSuccess = false;
  }

  /**
   * 
   * @param event Event from 
   */
  updatePicture(event: Event) {
    event.preventDefault();

    this.updateMessage = 'Uploading Picture';
    this.updateDetails = 'Picture uploading is in progress.. Please kindly wait!';
    this.upInPorgress = true;
    let profile_picture = this.profile.get('profile_picture').value;
    this.updateError = false ;
   

    this.http.updatePicture(profile_picture).subscribe(data => {
      // this.profile_img['nativeElement']['src']  = profile_picture ;
      ////console.log(data);
      this.upInPorgress = false;
      this.deleteMessage =  false ;
      this.updateSuccess = true;
      this.success = true ;

    }, (error: HttpErrorResponse) => {
      if (error.status == 502) {

        this.updateError = true;
        this.errorMessage = "Error Occured while trying to update profile picture, please try again later!";
        this.upInPorgress = false;
        setTimeout(() => {
          window.location.reload();
        }, 2000);

      } else if (error.status == 401 ) {
        ////console.log(error);
        
        this.updateError = true;
        this.errorMessage = "Error Occured while trying to update profile picture, please try again later!";
        this.upInPorgress = false;

        this.updatePicture(event) ;

        // setTimeout(() => {
        //   window.location.reload();
        // }, 2000);

      }else if (error.status == 400 ){
        this.upInPorgress =  false ;
        this.updateError = true;
        this.errorMessage = "No picture was selected. Please select a picture.";
      }


    })





  }



  deleteAccount() {

    this.deleteMessage = true;
    this.updateMessage = "Deleting Account";
    this.updateDetails = "...currently deleting your account. We will surely miss your presence here!";
    event.preventDefault();
    this.upInPorgress = true;
    this.http.deleteAccount().subscribe(data => {
      this.success = false ;
      this.upInPorgress = false;
      this.updateSuccess = true;
      this.router.navigate(["/login"]);
    }, (error: HttpErrorResponse) => {
      ////console.log(error);
      this.updateError = true;
      this.errorMessage = "Error Occured while trying to delete account , please try again later!";
      this.upInPorgress = false;


    })
  }

  cancel() {
    this.upInPorgress = false;
    this.showDeleteButton = false;
  }

  accountDeleteHelper(event: Event) {
    event.preventDefault();
    this.updateMessage = "Delete Account";
    this.updateDetails = "You are about to delete your account. This action is not reversible!";
    this.showDeleteButton = true;
    this.upInPorgress = true;
    this.showLoader = false;
  }




  previewFile(image: HTMLImageElement, inputFile: HTMLInputElement) {

    const file = inputFile['nativeElement']['files'][0];
    const reader = new FileReader();

    //once the data is beign loaded 
    reader.addEventListener("load", () => {
      // convert image file to base64 string


      image['nativeElement']['src'] = reader.result;

      this.profile.get('profile_picture').setValue(reader.result);

      ////console.log("Image Blob Format :::" + reader.result);

    }, false);

    if (file) {

      reader.readAsDataURL(file);

    }

  }

  updatePassword(event: Event) {

    this.updateMessage = 'Updating Account Password';
    this.updateDetails = 'Your account password update is in progress... please wait!';
    this.upInPorgress = true;
    event.preventDefault();
    this.upInPorgress = true;
    this.http.updatePassword(this.password.value).subscribe(data => {
      ////console.log(data);
      this.deleteMessage =  false ;
      this.success =  true ;
      this.upInPorgress = false;
      this.updateSuccess = true;
      this.successMessage = "Account password was successfully updated.";
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 2000);
    }, (error: HttpErrorResponse) => {
      if (error.status == 401) {
        //console.log(error);
        this.updateError = true;
        this.errorMessage = "Error Occured while trying to update account password, please try again later!";
        this.upInPorgress = false;
        setTimeout(() => {
          window.location.reload();
        }, 2000);

      }

    })
  }

  updateUsername(event: Event) {
    event.preventDefault();

    this.updateMessage = 'Updating Account Username';
    this.updateDetails = 'Your account username update is in progress... please wait!';
    this.upInPorgress = true;

    this.http.updateUsername(this.username.get('username').value).subscribe(data => {

      ////console.log(data);
      this.alreadyExists = true;
      this.upInPorgress = false;
      this.updateSuccess = true;

    }, (error: HttpErrorResponse) => {
      ////console.log(error);
      this.updateError = true;
      this.errorMessage = "Error Occured while trying to update account username, please try again later!";
      this.upInPorgress = false;
    })
=======
  constructor() { }

  ngOnInit(): void {
>>>>>>> first commit from server
  }

}
