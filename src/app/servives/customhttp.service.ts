import { Injectable } from '@angular/core';
<<<<<<< HEAD
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomcacheService } from './customcache.service';
import { tokenName } from '@angular/compiler';
=======
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomcacheService } from './customcache.service';
>>>>>>> first commit from server
@Injectable({
  providedIn: 'root'
})
export class CustomhttpService {
<<<<<<< HEAD
  // server: string = 'http://f1d2a6e3.ngrok.io/api/affiliate';
  server: string = 'https://tcodeinvestment.com/api/affiliate';
  headers: HttpHeaders;

  //  headers.append('Authorization', 'Bearer ' + localStorage.getItem("token"));
  //  headers.append('Content-Type', 'application/json');

  constructor(private http: HttpClient, private customCache: CustomcacheService) {
    this.headers = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      "Authorization": this.customCache.allInformation['token_type'] + " " + this.customCache.allInformation['access_token']
    });
  }

  //create new tap account 
  createNewUser(tapuser: Object): Observable<any> {
    //console.log("Before :::: " + tapuser);
    return this.http.post(this.server + "/register", tapuser, { responseType: 'json' }
    );
  }

  /******************** PASSWORD RESETS START ********************** */
  //send forgot password link
  forgotPasswordLink(email: object): Observable<any> {
    return this.http.post(this.server + "/forgot_password", email, { responseType: 'json', reportProgress: true })
  }
  //reset link should be giving 
  resetpassword(password: Object): Observable<any> {
    return this.http.post(this.server + "/reset_password_request", password, { responseType: 'json' });
  }
  resetPasswordWithToken(token: string, password: Object): Observable<any> {
    return this.http.post(this.server + `/update_password_with_token/` + token, password, { responseType: 'json' });
  }
  /**********************  PASSWORD RESETS ENDS ******************** */
=======
  server: "https://tcodeinvestment.com";
  token: "";
  constructor(private http: HttpClient, private customCache: CustomcacheService) {

  }

  //create new tap account 
  createNewUser(): Observable<any> {
    return this.http.post(this.server + "/tap/create/account", { responseType: 'text' }
    );
  }

  //send forgot password link
  forgotPasswordLink(): Observable<any> {
    return this.http.post(this.server + "/tap/forgotpassword/link", { responseType: 'text' })
  }
>>>>>>> first commit from server


  //send forgot password link
  signIn(loginDetails: Object): Observable<any> {
<<<<<<< HEAD
    return this.http.post(this.server + "/login", loginDetails, { responseType: 'json' });
  }



  //reset the account password 
  submitDetails(details: Object): Observable<any> {

    // //console.log( "ACCESS_TOKEN ::: " + this.customCache.access_token ) ;





    // //console.log( "ALL HEADERS SENT TO THE SERVER  :: " + this.getHeader().get('Authorization'));

    // //console.log( "SENDING  ::: " + this.customCache.token_type + " " + this.customCache.access_token );

    return this.http.post(this.server + "/upload_credentials", details, { responseType: 'json', headers: this.getHeader() });
  }

  verifyAccountActivation(verificationToken: string) {
    return this.http.get(this.server + "/verify_account/" + verificationToken, { responseType: 'json' });
  }





  fetchOngoingInvestment(): Observable<any> {
    return this.http.get(this.server + "/investments/ongoing", { responseType: 'json', headers: this.getHeader() });
  }



  fetchUnapprovedInvestment(): Observable<any> {
    return this.http.get(this.server + "/investments/unapproved", { responseType: 'json', headers: this.getHeader() });
  }



  fetchAbandonedInvestment(): Observable<any> {
    return this.http.get(this.server + "/investments/abandoned", { responseType: 'json', headers: this.getHeader() });
  }



  fetchCompletedInvestment(): Observable<any> {
    return this.http.get(this.server + "/investments/completed", { responseType: 'json', headers: this.getHeader() });
  }

  getHeader() {
    // return new HttpHeaders({
    //   'Accept': 'application/json',
    //   'Content-Type': 'application/json; charset=utf-8',
    //   "Authorization": JSON.parse(sessionStorage.getItem('tap_data'))['token_type'] + " " + JSON.parse(sessionStorage.getItem('tap_data'))['access_token']
    // }

    // //console.log(JSON.stringify ( this.customCache ) + "CUSTOM CACHE");
    // return new HttpHeaders({
    //   'Accept': 'application/json',
    //   'Content-Type': 'application/json; charset=utf-8',
    //   "Authorization": this.customCache.allInformation['token_type'] + " " + this.customCache.allInformation['access_token']
    // }

    // );
    // console.log(JSON.parse(sessionStorage.getItem("tap_data")));
    return this.headers;

  }


  /**
   * update the user account information
   */
  updateProfile(new_profile: Object): Observable<any> {
    return this.http.post(this.server + "/profile/update", new_profile, { responseType: 'json', headers: this.getHeader() });
  }


  /**
   * 
   * @param new_profile The profile picture of the user 
   */
  updatePicture(new_profile: string): Observable<any> {
    return this.http.post(this.server + "/profile_picture/update", { profile_img: new_profile }, { responseType: 'json', headers: this.getHeader() });
  }


  /**
   * 
   * @param new_profile The profile picture of the user 
   */
  deleteAccount(): Observable<any> {
    return this.http.post(this.server + "/delete/", { responseType: 'json', headers: this.getHeader() });
  }



  /**
 * 
 * @param new_profile The profile picture of the user 
 */
  checkUserExist(username: string): Observable<any> {
    return this.http.post(this.server + "/search/search_username", { username: username }, { responseType: 'json', headers: this.getHeader() });
  }


  /**
* 
* @param new_profile The profile picture of the user 
*/
  checkEmailExist(email: string): Observable<any> {
    return this.http.post(this.server + "/search/search_email", { email: email }, { responseType: 'json', headers: this.getHeader() });
  }

  /**
* 
* @param new_profile The profile picture of the user 
*/
  checkEmail(email: string): Observable<any> {
    return this.http.post(this.server + "/search/search_username", { email: email }, { responseType: 'json', headers: this.getHeader() });
  }



  /**
   * 
   * @param password Account password
   */
  updatePassword(password: Object): Observable<any> {
    return this.http.post(this.server + "/profile/update_password", password, { responseType: 'json', headers: this.getHeader() });
  }


  /**
* 
* @param new_profile The profile picture of the user 
*/
  updateUsername(username: string): Observable<any> {
    return this.http.post(this.server + "/profile/update_username", { username: username }, { responseType: 'json', headers: this.getHeader() });
  }


  /**
   * 
   * @param new_profile The profile picture of the user 
   */
  uploadBankProofOfPayment(hash: string, proof_of_payment: string): Observable<any> {
    //console.log("INVESTMENT HASH ::: " + hash);
    return this.http.post(this.server + "/investments/pay_with_transfer/" + hash, { proof_of_payment: proof_of_payment }, { responseType: 'json', headers: this.getHeader() });
  }


  /**
   * @returns investment hash 
   * @param new_investment The profile picture of the user 
   */
  sendNewInvestment(new_investment: Object): Observable<any> {
    return this.http.post(this.server + "/investments/start_new", new_investment, { responseType: 'json', headers: this.getHeader() });
  }


  /**
   * @returns investment hash 
   * @param new_investment The profile picture of the user 
   */
  sendMessageToAdmin(new_investment: Object): Observable<any> {
    return this.http.post(this.server + "/messages/send_message", new_investment, { responseType: 'json', headers: this.getHeader() });
  }


  /**
 * @returns investment hash 
 * @param new_investment The profile picture of the user 
 */
  receivedMessages(): Observable<any> {
    return this.http.get(this.server + "/messages/received", { responseType: 'json', headers: this.getHeader() });
  }


  /**
 * @returns investment hash 
 * @param new_investment The profile picture of the user 
 */
  fetchInvestmentStatics(): Observable<any> {
    console.log(this.getHeader().getAll);
    return this.http.get(this.server + "/investments/dashboard", { responseType: 'json', headers: this.getHeader() });
  }






=======
    return this.http.post(this.server + "/tap/signin", loginDetails, { responseType: 'json' });
  }

  //complted investment 
  completedInvestments() {
    return this.http.post(this.server + "/tap/" + this.customCache.username + "/investments/completed", { responseType: 'json' });
  }

  //abandoned invesment will be here
  abandonedInvestments() {
    return this.http.post(this.server + "/tap/" + this.customCache.username + "/investments/completed", { responseType: 'json' });
  }


  //ongoing investment 
  ongoingInvestments() {
    return this.http.post(this.server + "/tap/" + this.customCache.username + "/investments/completed", { responseType: 'json' });
  }

  //forgot password 
  forgotPassword() {
    return this.http.post(this.server + "/tap/" + this.customCache.username + "/investments/completed", { responseType: 'json' });
  }

  //reset the account password 
  resetpassword(username : string): Observable<any>{
    return this.http.post(this.server + "/tap/" + this.customCache.username + "/investments/completed", { responseType: 'json' });
  }

>>>>>>> first commit from server
}
