import { Component, OnInit, ViewContainerRef, ViewChild, Injector, ComponentFactoryResolver } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @ViewChild('centercontainer', { read: ViewContainerRef, static: true })
  private centercontainer: ViewContainerRef;

  constructor(private meta: Meta, private title: Title,
    private inj: Injector, private resolser: ComponentFactoryResolver) {

    }

  async ngOnInit() {
    const { CentercontainerComponent } = await import('../centercontainer/centercontainer.component');
    const centerContainerFactory = this.resolser.resolveComponentFactory(CentercontainerComponent);
    this.centercontainer.createComponent(centerContainerFactory, null, this.inj);
  }

}