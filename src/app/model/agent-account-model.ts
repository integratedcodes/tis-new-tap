

//account model for tap agent 
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class TapAgentAccountModel {
    
    surname: String;
    first_name: String;
    middle_name: String;
    sex: String;
    email: String;
    username: String;
    dob: String;
    password: String;
    cpassword: String;
    phone_number: String;
    occuapation : String ;
    has_uploaded : boolean ;
    is_approved : boolean ;
    is_verfified : boolean ;
    
}